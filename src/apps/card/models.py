from django.contrib.auth.models import User

from django.db import models


class Card(models.Model):
    """Model for card"""
    number = models.CharField(max_length=16, unique=True, verbose_name="Card Number")
    balance = models.IntegerField(default=0, verbose_name="Card Balance")
    user = models.ForeignKey(User, verbose_name="Card Owner", related_name="cards", on_delete=models.PROTECT)
    is_active = models.BooleanField(default=True, verbose_name='Active')

    def __str__(self):
        return self.number

    class Meta:
        app_label = 'card'
        db_table = 'card'
        verbose_name = "Card"
        verbose_name_plural = "Cards"
