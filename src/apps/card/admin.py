from django.contrib import admin

from apps.card.models import Card


class CardAdmin(admin.ModelAdmin):
    list_display = ['id', 'number', 'balance', 'is_active', 'user', ]
    save_on_top = True


admin.site.register(Card, CardAdmin)
