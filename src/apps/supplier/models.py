from django.db import models


class Denomination(models.Model):
    """Model for denominations """
    value = models.PositiveIntegerField(verbose_name="Denomination", unique=True)

    def __str__(self):
        return str(self.value)

    class Meta:
        app_label = 'supplier'
        db_table = 'denomination'
        verbose_name = "Denomination"
        verbose_name_plural = "Denominations"


class Supplier(models.Model):
    """Model for supplier"""
    name = models.CharField(max_length=255, verbose_name="Supplier Name")
    description = models.CharField(max_length=2500, blank=True, verbose_name="Supplier Description")
    certificate_rules = models.CharField(max_length=2500, blank=True, null=True, verbose_name="Certificate Rules")
    logo = models.ImageField(upload_to="logo/", verbose_name="Supplier Logo")
    contacts = models.JSONField(blank=True, null=True, verbose_name="Supplier Contacts")
    denominations = models.ManyToManyField(Denomination, related_name="suppliers", )

    def __str__(self):
        return self.name

    class Meta:
        app_label = 'supplier'
        db_table = 'supplier'
        verbose_name = "Supplier"
        verbose_name_plural = "Suppliers"
