# Generated by Django 4.0.6 on 2022-07-15 16:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('supplier', '0005_alter_supplier_denomination'),
    ]

    operations = [
        migrations.RenameField(
            model_name='supplier',
            old_name='denomination',
            new_name='denominations',
        ),
    ]
