from django.contrib import admin

from apps.certificate.models import Certificate
from apps.supplier.models import Supplier, Denomination


class SupplierAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'description', 'certificate_rules', 'logo', 'contacts', ]
    save_on_top = True


admin.site.register(Supplier, SupplierAdmin)
admin.site.register(Denomination, )
