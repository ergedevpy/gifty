from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from apps.supplier.models import Supplier, Denomination


class Certificate(models.Model):
    """Certificate model"""
    ENCODING = (
        ('EAN-8', 'EAN-8'),
        ('EAN-13', 'EAN-13'),
        ('EAN-14', 'EAN-14'),
    )

    code = models.CharField(max_length=12, unique=True, db_index=True, verbose_name="Certificate Code", )
    encoding = models.CharField(max_length=64, choices=ENCODING, default='EAN-13',
                                verbose_name="Certificate code encoding")
    denomination = models.ForeignKey(Denomination, on_delete=models.PROTECT,
                                     verbose_name="Certificate denomination",
                                     related_name="certificates")
    supplier = models.ForeignKey(Supplier, on_delete=models.PROTECT, related_name="certificates")

    def __str__(self):
        return self.code

    class Meta:
        app_label = 'certificate'
        db_table = 'certificate'
        verbose_name = "Certificate"
        verbose_name_plural = "Certificates"
