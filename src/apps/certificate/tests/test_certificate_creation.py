from django.test import TestCase

from apps.certificate.models import Certificate
from apps.supplier.models import Denomination, Supplier
from apps.utils.factories import CertificateFactory, SupplierFactory, DenominationFactory


class CreateCertificateTests(TestCase):
    """Test for certificate factory"""

    def setUp(self):
        SupplierFactory.create(name="TestSupplier", denominations={500: 1, 1000:2})

    def test_create_certificate(self):
        """Creating certificate"""
        denomination = Denomination.objects.get(value=500)
        supplier = Supplier.objects.first()
        certificate = Certificate.objects.first()

        self.assertEqual(certificate.denomination, denomination)
        self.assertEqual(certificate.supplier, supplier)
