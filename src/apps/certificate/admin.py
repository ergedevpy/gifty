from django.contrib import admin

from apps.certificate.models import Certificate


class CertificateAdmin(admin.ModelAdmin):
    list_display = ['id', 'code', 'encoding', 'denomination', 'supplier', ]
    save_on_top = True


admin.site.register(Certificate, CertificateAdmin)
