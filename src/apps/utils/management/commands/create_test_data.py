"""
Django command to create test data
"""
import factory
from django.core.management.base import BaseCommand

from apps.supplier.models import Denomination, Supplier
from apps.utils.factories import DenominationFactory, SupplierFactory, CardFactory, CertificateFactory


class Command(BaseCommand):
    """Django command to create test data"""
    help = u'Create test data'

    def add_arguments(self, parser):
        parser.add_argument('count', default=5, nargs='?', type=int, )

    def handle(self, *args, **kwargs):
        count = kwargs['count']

        CardFactory.create_batch(count)
        SupplierFactory.create_batch(count, denominations={1000: 2, 20000: 3, 30000: 4, 40000: 5, 5000: 6})

        self.stdout.write(self.style.SUCCESS("Test data created"))
