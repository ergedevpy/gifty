import random

import factory.fuzzy
from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from apps.card.models import Card
from apps.certificate.models import Certificate
from apps.supplier.models import Supplier, Denomination


class DenominationFactory(factory.django.DjangoModelFactory):
    value = factory.fuzzy.FuzzyInteger(1000, 5000, 1000)

    class Meta:
        model = Denomination


class CertificateFactory(factory.django.DjangoModelFactory):
    code = factory.fuzzy.FuzzyInteger(100000000000, 999999999999, 1)
    encoding = factory.fuzzy.FuzzyChoice(('EAN-8', 'EAN-13', 'EAN-14',))
    denomination = factory.Iterator(Denomination.objects.all())
    supplier = factory.Iterator(Supplier.objects.all())

    class Meta:
        model = Certificate


class SupplierFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('last_name')
    description = factory.Faker('text', max_nb_chars=random.randint(50, 100), )
    certificate_rules = factory.Faker('text', max_nb_chars=random.randint(50, 100), )
    logo = factory.django.ImageField(color='blue')

    @factory.post_generation
    def denominations(self, create, extracted):
        """
        Create denominations and certificates
        Parameters:
            extracted (dict(key:int, value:int)): Denomination and numbers of certificates which need create
        """
        if not create:
            return

        if extracted:

            for denomination, count in extracted.items():
                Denomination.objects.get_or_create(value=denomination)

                CertificateFactory.create_batch(count, supplier=self)

    class Meta:
        model = Supplier


class UserFactory(factory.django.DjangoModelFactory):
    email = factory.Faker('email')
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    username = factory.fuzzy.FuzzyText('user', 6, )

    class Meta:
        model = User


class CardFactory(factory.django.DjangoModelFactory):
    number = factory.fuzzy.FuzzyInteger(4100000000000000, 9999999999999999, 1)
    balance = factory.fuzzy.FuzzyInteger(0, 10000000, 10000)
    user = factory.SubFactory(UserFactory)
    is_active = factory.fuzzy.FuzzyInteger(0, 1, 1)

    class Meta:
        model = Card
