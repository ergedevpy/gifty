FROM python:3.10-alpine3.15

ENV PYTHONUNBUFFERED 1

COPY ./src /src
COPY pyproject.toml /src 
WORKDIR /src
ENV PYTHONPATH=${PYTHONPATH}:${PWD} 
EXPOSE 8000

RUN pip install --upgrade pip && \
    apk add --update --no-cache postgresql-client && \
    apk add --update --no-cache --virtual .tmp-build-deps \
        build-base postgresql-dev musl-dev && \
    apk add jpeg-dev zlib-dev libjpeg && \
    pip install poetry && \
    poetry config virtualenvs.create false && \
    poetry install --no-dev && \
    apk del .tmp-build-deps && \
    adduser \
    --disabled-password \
    --no-create-home \
    django-user


USER django-user

